variable broker_name {
  type        = string
  default     = ""
  description = "(Required) Name of the broker."
}

variable aws_mq_configuration_id {
  type        = string
  default     = ""
  description = "(Optional) The Configuration ID."
}

variable aws_mq_configuration_revision {
  type        = number
  default     = 1
  description = "(Optional) Revision of the Configuration."
}

variable engine_type {
  type        = string
  default     = ""
  description = "(Required) Type of broker engine. Valid values are ActiveMQ and RabbitMQ."
}

variable engine_version {
  type        = string
  default     = ""
  description = "(Required) Version of the broker engine. See the AmazonMQ Broker Engine docs for supported versions. For example, 5.17.6."
}

variable host_instance_type {
  type        = string
  default     = ""
  description = "(Required) Broker's instance type. For example, mq.t3.micro, mq.m5.large."
}

variable username {
  type        = string
  default     = ""
  description = "(Required) Configuration block for broker users. For engine_type of RabbitMQ, Amazon MQ does not return broker users preventing this resource from making user updates and drift detection. Detailed below."
}

variable password {
  type        = string
  default     = ""
  description = "(Required) Configuration block for broker users. For engine_type of RabbitMQ, Amazon MQ does not return broker users preventing this resource from making user updates and drift detection. Detailed below."
}

variable security_groups_ids {
  type        = list(string)
  default     = []
  description = "(Optional) List of security group IDs assigned to the broker."
}

variable auto_minor_version_upgrade {
  type        = bool
  default     = true
  description = "Optional) Whether to automatically upgrade to new minor versions of brokers as Amazon MQ makes releases available."
}

variable publicly_accessible {
  type        = bool
  default     = false
  description = "(Optional) Whether to enable connections from applications outside of the VPC that hosts the broker's subnets."
}

variable apply_immediately {
  type        = bool
  default     = false
  description = "(Optional) Specifies whether any broker modifications are applied immediately, or during the next maintenance window. Default is false."
}











