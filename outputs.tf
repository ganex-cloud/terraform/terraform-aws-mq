output "arn" {
  description = "ARN of the broker."
  value       = aws_mq_broker.main.arn
}

output "id" {
  description = "Unique ID that Amazon MQ generates for the broker."
  value       = aws_mq_broker.main.id
}

output "intances" {
  description = " List of information about allocated brokers (both active & standby)."
  value       = aws_mq_broker.main.instances
}