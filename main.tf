resource "aws_mq_broker" "main" {
  broker_name = var.broker_name

  configuration {
    id       = var.aws_mq_configuration_id
    revision = var.aws_mq_configuration_revision
  }

  engine_type                = var.engine_type
  engine_version             = var.engine_version
  host_instance_type         = var.host_instance_type
  security_groups            = var.security_groups_ids
  auto_minor_version_upgrade = var.auto_minor_version_upgrade
  publicly_accessible        = var.publicly_accessible
  apply_immediately          = var.apply_immediately

  user {
    username = var.username
    password = var.password
  }
}