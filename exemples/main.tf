module amozonmq-pje-dev {
  source                     = "../terraform-aws-mq"
  broker_name                = "pje-dev"
  engine_type                = "RabbitMQ"
  username                   = "teste"
  password                   = file(".secrets/amozonmq-pje-dev-password.txt")
  auto_minor_version_upgrade = true
  engine_version             = "3.11.28"
  host_instance_type         = "mq.t3.micro"
  publicly_accessible        = true
}